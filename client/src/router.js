import Vue from "vue";
import Router from "vue-router";
import buscador from "./views/Buscador";
import detalle from "./views/Detalle.vue";
import resultados from "./views/Resultados";

import Posts from '@/components/Posts';


Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "buscador",
      component: buscador
    },
    {
      path: "/items/:id",
      name: "detalle",      
      component: detalle, 
      props: true
    },
    {
      path: "/items",
      name: "resultados",
      component: resultados, 
      props: true
    },
    {
      path: '/posts',
      name: 'Posts',
      component: Posts
    }
  ]
});
