import Api from './Api'

export default {
  fetchItems (value) {
    return Api().get('items?search=' + value.search);
  },
  fetchItem (value) {
    return Api().get('items/' + value.id);
  }
}