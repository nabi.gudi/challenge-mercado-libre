# Challenge Mercado Libre

Test práctico para frontend de Mercado Libre. Se siguió un diseño dado y se consulta la API brindada por MeLi. 

## Para levantar client:
    $ cd client
    $ npm run serve

## Para levantar server:
    $ cd server
    $ npm run start