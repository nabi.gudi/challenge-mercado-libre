const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const fetch = require('node-fetch')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

app.get('/items', function(req, res) {
  let search = req.query.search;
  console.log(req.query);
  fetch('https://api.mercadolibre.com/sites/MLA/search?q='+search)
  .then((response) => { 
    return response.json();
  })
  .then((json) => {
    res.send(json);
  })
  .catch(error => {
    console.log(error)
  });
});

app.get('/items/:id', (req, res) => {
  var id, description;
  console.log(req.params.id+"hola");
  fetch('https://api.mercadolibre.com/items/'+req.params.id)
  .then((response) => { 
    console.log(response.json());
    id = response.json();
  })
  .then((json) => {
    id = json;
  })
  .catch(error => {
    console.log(error)
  });

  fetch('https://api.mercadolibre.com/items/'+req.params.id+'/description')
  .then((response) => { 
    description = response.json();
  })
  .then((json) => {
    description = json;
  })
  .catch(error => {
    console.log(error)
  });
  console.log(id);
  return id;
});

app.listen(process.env.PORT || 8081)